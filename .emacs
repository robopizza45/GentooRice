(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.org/packages/"))
(package-initialize)
;;------------------------------------------------------------------------
(use-package flycheck :ensure t)
(use-package org-bullets :ensure t)
(use-package chess :ensure t)
(use-package exwm :ensure t)
(use-package bongo :ensure t)
(use-package volume :ensure t)
(use-package emmet-mode :ensure t)
(use-package multiple-cursors :ensure t)
(use-package doom-themes :ensure t)
(use-package smex :ensure t)
(use-package try :ensure t)
(use-package magit :ensure t)
;;(use-package auctex :ensure t)
(use-package latex-preview-pane :ensure t)
;;------------------------------------------------------------------------
(require 'server)
(unless (server-running-p)
  (server-start))
;;------------------------------------------------------------------------
(global-set-key (kbd "C-x C-k") 'next-buffer)
(global-set-key (kbd "C-x C-j") 'previous-buffer)
(global-set-key (kbd "C-c C-b") 'bongo)
(global-set-key (kbd "C-c C-v") 'volume)
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
;;------------------------------------------------------------------------
(load-theme 'doom-one t)
(display-time)
(setq org-todo-keywords
      '((sequence "TODO" "In-Progress" "Scheduled" "Done" "Waiting")))
(setq backup-inhibited t)
(setq auto-save-default nil)
(setq uesr-full-name "Rinzler")
(setq display-time-24hr-format t) 
(setq doc-view-continuous t) 
(setq visible-bell t)
(setq compilation-read-read-command nil)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
(setq revert-without-query '(".*"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Hack" :foundry "simp" :slant normal :weight normal :height 100 :width normal)))))
;;------------------------------------------------------------------------
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(TeX-show-compilation nil)
 '(display-time-mode t)
 '(doc-view-resolution 110)
 '(inhibit-startup-buffer-menu nil)
 '(inhibit-startup-screen t)
 '(initial-buffer-choice (quote remember-notes))
 '(initial-major-mode (quote remember-notes))
 '(latex-preview-pane-enable nil)
 '(menu-bar-mode nil)
 '(package-enable-at-startup nil)
 '(package-selected-packages
   (quote
    (flycheck jdee org-bullets emms multiple-cursors try volume magit chess auctex latex-preview-pane smex use-package org-edna exwm emmet-mode doom-themes bongo)))
 '(scroll-bar-mode nil)
 '(tool-bar-mode nil)
 '(tooltip-mode nil))
