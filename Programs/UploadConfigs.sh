#!/bin/bash

cp -r ~/.emacs ../.emacs
cp -r ~/.config/bspwm ../.config
cp -r ~/.config/sxhkd/sxhkdrc ../.config
cp -r ~/.config/lock.sh ../.config/lock.sh
cp -r ~/.config/polybar ../.config
cp -r ~/.ncmpcpp ../.ncmpcpp
cp -r ~/.config/rofi/ ../.config
cp -r ~/.config/mpd ../.config
cp -r ~/.config/neofetch ../.config
cp -r ~/.config/ranger ../.config
cp -r ~/.Xdefaults ../GentooRice/
cp -r ~/Programs/restart.sh ../Programs/
#cp -r ~/Pictures ../GentooRice
cp -r ~/.calcurse ../GentooRice
cp -r ~/.compton.conf .
cp -r /etc/mpd.conf ..//MPDConfig
cp -r /etc/portage/make.conf ../GentooConfigs/make.conf
cp -r /etc/portage/package.use/package.use ../GentooConfigs/package.use
cp -r /var/lib/portage/world ../GentooConfigs/world
cp -r /etc/portage/repos.conf/gentoo.conf ../GentooConfigs/repos.conf
#cp -r ~/Documents/KernelConfig ../GentooConfigs/KernelConfig

#git add .
#STATUS="$(git status)"
#echo "${STATUS}"
#git commit -m "Updates"
#git push origin master
