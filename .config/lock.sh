#!/bin/bash
LOCK=$HOME/.config/polybar/lock.png
TMPIMG=/tmp/screen.png
scrot /tmp/screen.png
#convert $TMPIMG -blur 0x5 $TMPIMG
convert $TMPIMG -scale 10% -scale 1000% $TMPIMG
convert $TMPIMG $LOCK -gravity center -composite -matte $TMPIMG
i3lock -u -i $TMPIMG
rm /tmp/screen.png
